#!/usr/bin/env bash

if command -v nginx 2>/dev/null; then
    echo ">> nginx already installed"
else
    echo ">> installing nginx ..."

    sudo yum install -y epel-release
    sudo yum install -y nginx

    sudo systemctl start nginx
    sudo systemctl enable nginx
fi